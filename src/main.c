/*
Un file contiene una sequenza (di lunghezza ignota) di numeri interi positivi,
uno per riga. I numeri sono scritti in lettere cifra per cifra, e sono terminati dalla parola stop. Come
esempio si consideri il seguente file:

otto cinque nove stop
due due stop
sette zero sette stop
Il file contiene i numeri 859, 22 e 707.

Si scriva un programma C, opportunamente organizzato in funzioni, che legga da tastiera il nome del
file e stampi il valore della somma dei numeri ivi contenuti. Nell’esempio il programma deve stampare
1588.

Si assuma che i numeri siano di dimensione tale da essere rappresentabili tramite il tipo int.
*/
#include "base_include.h"

void saveOut(int sum);
int SommaInteri(char nome_file[]);
int Valore(char cifra_lettere[]);

int main(int argc, char *argv[])
{
	// Get file data
	int sum = SommaInteri("data/input.txt");
	saveOut(sum);
	
	return 0;
}

void saveOut(int sum)
{
	// Save sum in file out.txt
	FILE *out = fopen("data/out.txt", "w");
	fprintf(out, "Out: %d", sum);
	fclose(out);

	printf("Data saved in: ./data/out.txt\n");

	FILE *in = fopen("data/out.txt", "r");
	char string[1024];

	while (!feof(in))
	{
		//fgets(string, 50, in);
		fscanf(in, "%[^\n]s", string);
	}

	printf("File ./data/out.txt: %s\n", string);
}

int SommaInteri(char nome_file[])
{
	int somma = 0, num;
	char cifra_lettere[8];
	FILE *fp;
	fp = fopen(nome_file, "r");
	while (fscanf(fp, "%s", cifra_lettere) != EOF)
	{
		num = 0;
		while (strcmp(cifra_lettere, "stop") != 0)
		{
			num = num * 10 + Valore(cifra_lettere);
			fscanf(fp, "%s", cifra_lettere);
		}
		somma += num;
	}
	fclose(fp);
	return somma;
}

int Valore(char cifra_lettere[])
{
	if (strcmp(cifra_lettere, "uno") == 0)
		return 1;
	else if (strcmp(cifra_lettere, "due") == 0)
		return 2;
	else if (strcmp(cifra_lettere, "tre") == 0)
		return 3;
	else if (strcmp(cifra_lettere, "quattro") == 0)
		return 4;
	else if (strcmp(cifra_lettere, "cinque") == 0)
		return 5;
	else if (strcmp(cifra_lettere, "sei") == 0)
		return 6;
	else if (strcmp(cifra_lettere, "sette") == 0)
		return 7;
	else if (strcmp(cifra_lettere, "otto") == 0)
		return 8;
	else if (strcmp(cifra_lettere, "nove") == 0)
		return 9;
	else
		return 0;
}